// jQuery plugins
// @codekit-prepend "../bower_components/fitvids/jquery.fitvids.js"

(function($) {
	$('body').fitVids();
})(jQuery);

// patterns includes
// @codekit-append "patterns/collapser.js"
