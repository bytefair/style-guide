# Sass Coding Guidelines

## Principles of Development
1. Avoid styling with IDs. You should never use them in CSS.
2. All styles should have a single layer of specificity and should never be nested except to scope styles inside a block.
3. Components should be as generic as possible to promote reuse.

## Style Guidelines
These guidelines should be considered best practices and are always up for changes. You can only decide best practice by usage so if something is problematic we can change it at will. Please provide feedback to other developers on usage.

### General Formatting
**file names:** Always name assets using hyphens to replace whitespace unless otherwise required. File names should always be in lowercase. Use prefixes to seperate files into logical groups. File names should make clear what that file is used for. Do not use a generic name unless it's a generic component.

```
Preferred names:
bg-contact.jpg
bg-home.jpg
bg-products.jpg
icon-home.png
icon-mail.png
sprite-site-navigation.jpg

Bad names:
contact-bg.jpg
bg_home.jpg
bg.jpg
iconHome.png
iconmail.png
```

Sass files should be sorted into folders for different purposes. They should be very specific to the styles they contain. It's easier to deal with 20 SCSS files than a file with 20 classes. If you are compiling with source maps, you'll be able to see in your inspector exactly which file the style is defined in.

---

**folder structure:** There are many arrangements for organizing Sass files. It's very likely that your project will have too many files to keep them all in one folder, so the following folders are suggested:

* base - global styles
* elements - specific elements and patterns
* pages - page-specific styles, almost always scoped

---

**class names:** Class names for reusable elements should be as generic as possible and section specific elements should be prefixed with block. It's far better to style something like `.site-menu__menu-item` than `.site-menu .menu-item`. The latter has far more potential for problems as well as demonstrably worse rendering performance.

BEM style class names are great because you can tell more about the context at a single glance. It's obvious if you use BEM style the fundamental difference between `.div__active` and `.div--active`. The former is a nested style and the latter is a modifier.

Related classes should be defined on the next line after the closing bracket and unrelated classes should have at least one extra line. Some like to use their closing bracket on the final line of the declaration but this is not preferred due to common CSS style.

---

**comments:** Use PHPDoc-style comments for multiline comment blocks used for documentation purposes. Use double slashes for single line comments. Comments headers are styled with the four corner flag method (see example). You usually will want more space (3 or 4 lines even) before making a four corner flag with approx 40 asterisks. All comments will be erased from compressed Sass aside from loud comments so you don't need to worry about compile inclusion.

---

**width formatting**: Prefer 80 columns for all stylesheets and comment blocks (excepting unbreakable strings like URLs). Avoid making declarations on a single line.

---

**attribute ordering:** Consistency is key. You can use alphabetical organization if you want or you can try something like box model and positioning settings first. It's up to you, just make it consistent so people can find your attributes.

### Attribute Values
**colors:** RGB/RGBA format is preferred as it is most compatible with other programs.

**attribute quoting:** Quote strings in attributes like URLs and font names for clarity and ease of searching.

**numbers:** Any floating point value less than one should have a leading zero. Floating points should be calculated to at least 5 digits and more is always better but do make them standard.

**shorthand declarations:** It's preferable to split up shorthand declarations like `background` whenever possible as it makes the code more reusable, transparent, and easy to grok. Use shorthand with extreme caution.

**quotes:** Use double quotes in defs.

```scss
/****************************************\
    First Section Header
\****************************************/

/**
 * Extended PHPDoc-like comment block
 *
 * This is used for extended documentation, mixins, and functions.
 */
.class-name,
.class-name2 {
    background-color: rgb(0, 0, 0);
}
.class-name:hover {
    background-color: rgb(12, 12, 12);
}
.class-name__span {
    opacity: 0.85;
}
.class-name__span--modified {
    opacity: 0.75;
}

// a shorter one line comment
.button {
    background-color: rgb(200, 200, 200);
    border-radius: 4px;
    border-width: 2px;
}
.button--active {
    background-color: rgb(0, 128, 0);
}




/****************************************\
    Second Section Header
\****************************************/
.class-name {
    color: rgba(0, 0, 0, 0.55);
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
    background-image: url("images/bg.jpg");
}
.class-name:hover {
    color: rgba(0, 0, 0, 0.85);
}
```

### Dealing with Mixins and Extends
Mixins and extends are powerful and should be used judiciously and correctly.

Extends should only be used with extreme caution because you're going to cause possible problems in the cascade. Remember extends stay in place, so if you define those at the beginning of your document, they can be overridden easily. This leads to confusing behavior, so it's better to avoid these in general usage. The place to use them is in defining objects, so you can both reduce the amount of extraneous CSS classes in your templates and avoid the confusion of cascade ambiguity.

You can use mixins freely from a perspective of cascade and should have no problems, but you're also far from best practice if you're constantly reproducing code. Use mixins when you MUST override the whatever is in the cascade, such as font settings. They should not be used in the example case, for instance, because it would duplicate the code for every single button although from a working standpoint, it probably doesn't matter.

```scss
.button {
    border-radius: 4px;
    border-width: 1px;
}
.button--accept { // use "button--accept" only instead of both classes
    @extend .button;
    background-color: rgb(0, 180, 0);
}
```

### Vendor Prefixes ###
Use [Autoprefixer](https://github.com/postcss/autoprefixer) which is also built into CodeKit. It uses [Can I Use](http://caniuse.com/) to make proper decisions about which prefixes to include. The community is smarter than you, take advantage of it. It's also 100% configurable both at the command line and in CodeKit.

## Meta-Framework Components

### CSS Reset
There is a very minimal CSS reset used on top of Normalize.css to reset a few default browser styles.

```scss
// always border-box
*, *:before, *:after {
    -webkit-box-sizing: border-box;
       -moz-box-sizing: border-box;
            box-sizing: border-box;
}

// all block elements
address, article, aside, audio, blockquote, canvas, dd, div, dl, fieldset, figcaption, figure, footer, form, h1, h2, h3, h4, h5, h6, header, hgroup, hr, noscript, ol, output, p, pre, section, table, tfoot, ul, video {
    margin: 0;
    padding: 0;
    text-rendering: optimizeLegibility;
}
h1, h2, h3, h4, h5, h6 {
    font-size: 1em;
    font-weight: normal;
}

// more sane presets for lists
ul, ol {
    padding-left: 1em;
}
dd {
    margin-left: 1em;
}
figure {
    margin: 0;
}
img {
    max-width: 100%;
}
pre code {
    tab-size: 4;
}

// forms reset
fieldset {
    margin: 10px;
    padding: 10px;
    legend {
        font-weight: bold;
    }
    ul {
        padding-left: 10px;
    }
    li {
        list-style-type: none;
    }
}
label {
    display: block;
}
input[type="email"], input[type="number"], input[type="password"], input[type="search"], input[type="tel"], input[type="text"], input[type="url"], input[type="color"], input[type="date"], input[type="datetime"], input[type="datetime-local"], input[type="month"], input[type="time"], input[type="week"], input[type="range"], textarea, select {
    min-width: 250px;
}
```

### Predefined z-index
`z-index` can be very confusing, so we've made a small series of values for you to automatically assign to different elements based on their function. The actual `z-index` values are 100x multiples of their level. They are set in one file with a default defintion for easy override if necessary. If you absolutely require finer control, you can pass a "tens" to the z-index mixin as a second argument.

* **Level 1** - Default foreground content
* **Level 2** - In-page captions, figures
* **Level 3** - Logos, buttons
* **Level 4** - Block-specific controls or metabars
* **Level 5** - Block-specific overlay
* **Level 6** - Navigation
* **Level 7** - Tooltips, popovers, modals
* **Level 8** - Loading or progress bars
* **Level 9** - Full-screen overlays
* **Level 10** - Development/debugging

```scss
body {
    @include z-index(1);     // z-index is 100
}

.button {
    @include z-index(3);     // z-index is 300
}
.button--approve {
    @include z-index(3, 12); // z-index is 312
}
```
