<?php include_once 'functions.php'; ?>

<html lang="en-US">
	<head>
		<meta charset="UTF-8">
		<title><?php echo $doctitle; ?></title>
		<link rel="stylesheet" href="compiled/master.css">
		<script src="assets/bower_components/jquery/dist/jquery.min.js"></script>
	</head>
	<body class="<?php echo filevar(); ?>">
		<header class="site-header">
			<h1 class="site-header__title"><?php echo $doctitle; ?></h1>
			<nav class="site-header__nav">
				<ul>
					<li><a href="/base.php">Base Styles</a></li>
					<li><a href="/patterns.php">UI Patterns</a></li>
					<li><a href="/forms.php">Form Styles</a></li>
					<li><a href="/docs/">Style Docs</a></li>
				</ul>
			</nav>
		</header>
