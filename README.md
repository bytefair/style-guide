# Coolfire Style Guide
Style Guide is a minimalist app used in the creation of style guides for Coolfire Studios websites. It contains a small command line tool that uses Thor to generate unique UI patterns. It also houses some documentation surrounding style code.

### Creative Commons Licensed Files
Photos in images folder - Creative Commons BY-NC-SA 2.0 by https://www.flickr.com/photos/blmiers2/
