<?php
/**
 * functions.php
 *
 * Globally available function definitions
 */

function filevar() {
	return str_replace('.php', '', basename($_SERVER['PHP_SELF']));
}

function pattern_name($file) {
	return str_replace('.html', '', $file);
}
