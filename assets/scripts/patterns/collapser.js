(function($) {
	$('.collapser').on('click', '.collapser__title', function() {
		$(this).siblings('.collapser__body').slideToggle();
	});
})(jQuery);
