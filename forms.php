<?php
/**
 * forms.php
 *
 * Form styles
 */

$doctitle = "Form Styles";

include_once 'includes/header.php';
?>

<main class="form-styles">

	<form>
		<fieldset>
			<p>
				<label for="text">Text Input (required)</label>
				<input type="text" id="text" required>
			</p>
			<p>
				<label for="password">Password (required)</label>
				<input type="password" id="password" required>
			</p>
			<p>
				<label for="email">Email Address (required)</label>
				<input type="email" id="email" placeholder="user@domain.com" required autocomplete>
			</p>
			<p>
				<label for="website">Website (Text Input with Pattern)</label>
				<input type="url" id="website" placeholder="http://www.domain.com" pattern="^https?://([a-zA-Z0-9]([a-zA-Z0-9\-]{0,61}[a-zA-Z0-9])?\.)+[a-zA-Z]{2,6}$" autocomplete>
			</p>
			<p>
				<label for="texarea">Textarea</label>
				<textarea id="textarea"></textarea>
			</p>
			<p>
				<label for="select">Select</label>
				<select id="select">
					<optgroup label="Colors">
						<option>Blue</option>
						<option>Red</option>
						<option>Yellow</option>
						<option>Pink</option>
					</optgroup>
					<optgroup label="Shapes">
						<option>Square</option>
						<option>Circle</option>
						<option>Triangle</option>
					</optgroup>
				</select>
			</p>
			<p>
				<label for="multiselect">Multi-select</label>
				<select type="select" multiple>
					<option>Taco</option>
					<option>Burrito</option>
					<option>Pizza</option>
					<option>Corn Dog</option>
					<option>Funnel Cake</option>
				</select>
			</p>
			<p>
				<label for="range">Range</label>
				<input type="range" name="range" min="0" max="10" value="1">
			</p>
			<p>
				<label for="file">File Upload</label>
				<input type="file" id="file">
			</p>
			<fieldset>
				<legend>Checkboxes</legend>
					<ul>
						<li>
							<label for="checkbox1">
								<input type="checkbox" id="checkbox1" name="checkbox" checked> Pre-checked
							</label>
						</li>
						<li>
							<label for="checkbox2">
								<input type="checkbox" id="checkbox2" name="checkbox"> Unchecked
							</label>
						</li>
					</ul>
			</fieldset>
			<fieldset>
				<legend>Radio Select</legend>
					<ul>
						<li>
							<label for="radio1">
								<input type="radio" id="radio1" name="radio"> Option 1
							</label>
						</li>
						<li>
							<label for="radio2">
								<input type="radio" id="radio2" name="radio"> Option 2
							</label>
						</li>
						<li>
							<label for="radio3">
								<input type="radio" id="radio3" name="radio"> Option 3
							</label>
						</li>
					</ul>
			</fieldset>
			<fieldset>
				<legend>Button Types</legend>
				<input type="submit" value="Submit Form">
				<input type="button" value="Preview Form">
				<a href="#">Cancel</a>
			</fieldset>
		</fieldset>
	</form>

</main>

<?php
include_once 'includes/footer.php';
