<?php
/**
 * base.php
 *
 * Base styles and typography
 */

$doctitle = "Base Styles and Typography";

include_once 'includes/header.php';
?>



<main class="base-styles">

	<section class="base-styles__section">
		<h1 class="base-styles__section__title">Headings</h1><?php

		$heading_text = 'A Test Header Text and <a href="#">Link Demonstration</a>'; ?>

		<h1>h1. <?php echo $heading_text; ?></h1>
		<h2>h2. <?php echo $heading_text; ?></h2>
		<h3>h3. <?php echo $heading_text; ?></h3>
		<h4>h4. <?php echo $heading_text; ?></h4>
		<h5>h5. <?php echo $heading_text; ?></h5>
		<h6>h6. <?php echo $heading_text; ?></h6>
	</section>


	<section class="base-styles__section">
		<h1 class="base-styles__section__title">Main Copy Styles</h1>

		<h2>h2. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</h2>

		<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.</p>

		<p>In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus.</p>

		<h3>h3. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</h3>

		<p>Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem.</p>

		<p>Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien.</p>

		<h4>h4. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</h4>

		<p>Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia. Nam pretium turpis et arcu. Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Sed aliquam ultrices mauris. Integer ante arcu, accumsan a, consectetuer eget, posuere ut, mauris. Praesent adipiscing. Phasellus ullamcorper ipsum rutrum nunc. Nunc nonummy metus. Vestibulum volutpat pretium libero. Cras id dui. Aenean ut</p>
	</section>


	<section class="base-styles__section">
		<h1 class="base-styles__section__title">Horizontal Rule</h1>
		<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.</p>

		<hr>

		<p>In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus.</p>
	</section>


	<section class="base-styles__section">
		<h1 class="base-styles__section__title">Pre-formatted Text</h1>

		<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.</p>

		<pre>&#32; ! " # $ % &amp; ' ( ) * + , - . /
0 1 2 3 4 5 6 7 8 9 : ; &lt; = &gt; ?
@ A B C D E F G H I J K L M N O
P Q R S T U V W X Y Z [ \ ] ^ _
` a b c d e f g h i j k l m n o
p q r s t u v w x y z { | } ~</pre>

		<h2 class="base-styles__subsection__title">Pre-formatted Code</h2>

		<p>In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus.</p>

		<pre><code>body {
	color: #292F33;
	font-size: 14px;
	line-height: 18px;
}</code></pre>
	</section>


	<section class="base-styles__subsection">
		<h1 class="base-styles__subsection__subtitle">Quotes</h1>

		<h2 class="base-styles__subsection__title">Blockquote with Citation</h2>
		<blockquote>Adapt or perish, now as ever, is nature's inexorable imperative.&mdash;H. G. Wells</blockquote>

		<h2 class="base-styles__subsection__title">Inline Quote</h2>
		<p>Kenny's deaths are well known in popular culture, and was one of the things viewers most commonly associated with South Park during its earlier seasons. Everytime Kenny is killed, Stan will announce <q cite="http://en.wikipedia.org/wiki/Kenny_McCormick#Cultural_impact">Oh my God, you/they killed Kenny!</q>.
</p>
	</section>


	<section class="base-styles__subsection">
		<h1 class="base-styles__subsection__title">List Types</h1>

		<h2 class="base-styles__subsection__title">Unordered List</h2>
		<ul>
			<li>Illinois</li>
			<li>Kansas</li>
			<li>Missouri
				<ul>
					<li>Saint Louis</li>
					<li>Kansas City</li>
				</ul>
			</li>
			<li>Michigan</li>
		</ul>

		<h2 class="base-styles__subsection__title">Ordered List</h2>
		<ol>
			<li>First item</li>
			<li>Second item</li>
			<li>Third item with sub-list
				<ol>
					<li>Sub item 1</li>
					<li>Sub item 2</li>
				</ol>
			</li>
		</ol>

		<h2 class="base-styles__subsection__title">Definition List</h2>
		<dl>
			<dt>HTML</dt>
			<dd>1. A markup language used to describe web documents</dd>
			<dd>2. How To Meet Ladies</dd>
			<dt>CSS</dt>
			<dt>Cascading Style Sheets</dt>
			<dd>A language used to describe styles for HTML</dd>
			<dt>JavaScript</dt>
			<dd>A programming language most commonly used to manipulate HTML</dd>
		</dl>
	</section>


	<section class="base-styles__subsection">
		<h1 class="base-styles__subsection__title">Figures</h1>

		<h2 class="base-styles__subsection__title">Traditional Figure and Caption</h2>
		<figure>
			<img src="assets/images/denali.jpg">
			<figcaption>
				<a href="https://www.flickr.com/photos/blmiers2/6167391543/in/photolist-aoZuCX-a1e2C4-anNxDr-aGsvBT-bk27BD-7iFC5F-bw9Gh8-ajgMSW-aGsKED-akjdM9-a1vEKq-am8qmY-dBzHBu-cSL8jY-8uBSyy-bss6pk-akCWUJ-anjKYX-akzyGd-am65ec-Fuzsj-5Egvcb-am5khR-bqiqRr-cL5os7-ajcrts-4mi1mx-bheeuM-4K5wFz-dZFYWK-m3UCyj-ntcYio-ajft2W-4US4SD-aabrJQ-yM1B-h6c59q-f4JGso-ai2Q6U-9coH2-6Yfycn-ajfFEA-7GLjVQ-5rxhtK-e92Bbf-5RhVas-j59Qzz-7BPmTG-gnuUS6-6qdodF/">Mountain - Alaska's Denali</a>
			</figcaption>
		</figure>

		<h2 class="base-styles__subsection__title">Paragraph with Citation</h2>
		<figure>
			<p>‘Twas brillig, and the slithy toves<br>
			Did gyre and gimble in the wabe;<br>
			All mimsy were the borogoves,<br>
			And the mome raths outgrabe.</p>
			<figcaption>
				<cite>Jabberwocky</cite> (first verse). Lewis Carroll, 1832-98
			</figcaption>
		</figure>

		<h2 class="base-styles__subsection__title">Blockquote with Citation</h2>
		<figure>
			<blockquote cite="http://hansard.millbanksystems.com/commons/1947/nov/11/parliament-bill#column_206">
				<p>Many forms of Government have been tried, and will be tried in this world of sin and woe. No one pretends that democracy is perfect or all-wise. Indeed, it has been said that democracy is the worst form of government except all those other forms that have been tried from time to time.</p>
			</blockquote>
			<figcaption>
				Winston Churchill, in <cite><a href="http://hansard.millbanksystems.com/commons/1947/nov/11/parliament-bill#column_206">a speech to the House of Commons</a></cite>. 11th November 1947
			</figcaption>
		</figure>
	</section>


	<section class="base-styles__subsection">
		<h1 class="base-styles__subsection__title">Semantic Text Styles</h1>

		<h2 class="base-styles__subsection__subtitle">Links</h2>
		<p>The HTML Anchor Element defines a <a href="#">hyperlink</a>, the named <a href="#">target destination</a> for a hyperlink, or both.</p>

		<h2 class="base-styles__subsection__subtitle">Stressed Emphasis</h2>
		<p>In HTML 5, what was previously called <em>block-level</em> content is now called <em>flow</em> content.</p>

		<h2 class="base-styles__subsection__subtitle">Strong Importance</h2>
		<p>When doing x it is <strong>imperative</strong> to do y before proceeding.</p>

		<h2 class="base-styles__subsection__subtitle">Small Print</h2>
		<p><small>&copy; 2014 Coolfire Studios, All Rights Reserved</small></p>

		<h2 class="base-styles__subsection__subtitle">Strikethrough</h2>
		<p><s>Today's Special: Salmon</s> SOLD OUT<br /></p>

		<h2 class="base-styles__subsection__subtitle">Citations</h2>
		<p>More information can be found in <cite>[ISO-0000]</cite>.</p>

		<h2 class="base-styles__subsection__subtitle">Inline Quotes</h2>
		<p>Everytime Kenny is killed, Stan will announce <q cite="http://en.wikipedia.org/wiki/Kenny_McCormick#Cultural_impact">Oh my God, you/they killed Kenny!</q>.</p>

		<h2 class="base-styles__subsection__subtitle">Definitions</h2>
		<p><dfn title="a global system of interconnected networks that use the Internet Protocol Suite">The Internet</dfn> is a global system of interconnected networks that use the Internet Protocol Suite (TCP/IP) to serve billions of users worldwide.</p>

		<h2 class="base-styles__subsection__subtitle">Abbreviation</h2>
		<p>The <abbr title="HyperText Markup Language">HTML</abbr> Abbreviation Element represents an abbreviation and optionally provides a full description for it. If present, the title attribute must contain this full description and nothing else.</p>

		<h2 class="base-styles__subsection__subtitle">Time</h2>
		<p>The concert took place on <time datetime="2001-05-15 19:00">May 15</time>.</p>

		<h2 class="base-styles__subsection__subtitle">Code</h2>
		<p>Regular text. <code>This is inline code.</code> Regular text.</p>

		<h2 class="base-styles__subsection__subtitle">Variable</h2>
		<p> A simple equation: <var>x</var> = <var>y</var> + 2 </p>

		<h2 class="base-styles__subsection__subtitle">Sample Output</h2>
		<p>Regular text. <samp>This is sample text.</samp> Regular text.</p>

		<h2 class="base-styles__subsection__subtitle">Keyboard Entry</h2>
		<p>Save the document by pressing <kbd><kbd>Ctrl</kbd>+<kbd>S</kbd></kbd></p>

		<h2 class="base-styles__subsection__subtitle">Superscript and Subscript</h2>
		<p>This text is <sup>superscripted</sup></p>
		<p>The chemical formula of peroxide is H<sub>2</sub>O<sub>2</sub>. This text is <sub>subscripted</sub></p>

		<h2 class="base-styles__subsection__subtitle">Italicized</h2>

		<p>The Latin phrase <i>Veni, vidi, vici</i> is often mentioned in music, art, and literature</p>

		<h2 class="base-styles__subsection__subtitle">Bold</h2>
		<p>This article describes several <b>text-level</b> elements. It explains their usage in an <b>HTML</b> document.</p>

		<h2 class="base-styles__subsection__subtitle">Marked/Highlighted</h2>
		<p>The &lt;mark&gt; element is used to <mark>highlight</mark> text</p>

		<h2 class="base-styles__subsection__subtitle">Edited</h2>
		<p><del>This text has been deleted</del> <ins>This text has been inserted</ins></p>

	</section>

	<section class="base-styles__subsection">
		<h1 class="base-styles__subsection__title">Tables</h1>
		<p>Tabular data should be encoded with <code>thead</code>, <code>tfoot</code>, and <code>tbody</code> elements in that order. The table footer should be read by browser before the data is actually fed in.</p>

		<table>
			<caption><a href="http://paleomg.com/guest-post-mini-chocolate-waffles/">Mini Chocolate Waffles</a></caption>
			<colgroup>
				<col>
				<col>
				<col>
			</colgroup>
			<thead>
				<tr>
					<th scope="col">Ingredients</th>
					<th scope="col">Single Batch</th>
					<th scope="col">Double Batch</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>coconut flour</td>
					<td>&frac14; cup</td>
					<td>&frac12; cup</td>
				</tr>
				<tr>
					<td>cocoa powder</td>
					<td>&frac14; cup</td>
					<td>&frac12; cup</td>
				</tr>
				<tr>
					<td>baking soda</td>
					<td>&frac14; tsp</td>
					<td>&frac12; tsp</td>
				</tr>
				<tr>
					<td>sea salt</td>
					<td>&frac14; tsp</td>
					<td>&frac12; tsp</td>
				</tr>
				<tr>
					<td>eggs</td>
					<td>3</td>
					<td>6</td>
				</tr>
				<tr>
					<td>coconut milk</td>
					<td>&frac14; cup</td>
					<td>&frac12; cup</td>
				</tr>
				<tr>
					<td>ghee or coconut oil</td>
					<td>2 tbsp</td>
					<td>4 tbsp</td>
				</tr>
				<tr>
					<td>maple syrup</td>
					<td>3 tbsp</td>
					<td>6 tbsp</td>
				</tr>
				<tr>
					<td>vanilla extract</td>
					<td>1 tsp</td>
					<td>2 tsp</td>
				</tr>
			</tbody>
		</table>
	</section>


	<section class="base-styles__subsection">
		<h1 class="base-styles__subsection__title">Embeds</h1>

		<h2 class="base-styles__subection__subtitle">YouTube</h2>
		<iframe width="560" height="315" src="//www.youtube.com/embed/ZMByI4s-D-Y" frameborder="0" allowfullscreen></iframe>

		<h2 class="base-styles__subection__subtitle">Vimeo</h2>
		<iframe src="//player.vimeo.com/video/93491995" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
	</section>

</main>



<?php
include_once 'includes/footer.php';
