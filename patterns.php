<?php
/**
 * patterns.php
 *
 * UI patterns
 */

$doctitle = "UI Patterns";

include_once 'includes/header.php';
?>

<main class="ui-patterns">

<?php

	$files        = array();
	$patterns_dir = "patterns";
	$handle       = opendir($patterns_dir);

	while ( false !== ($file = readdir($handle)) ) :
		if ( stristr($file,'.html') ) :
			$files[] = $file;
		endif;
	endwhile;
	sort($files);

	/**
	 * Pattern builder
	 *
	 * Compiles all HTML files in the patterns directory
	 */ ?>
	<?php foreach ($files as $file) :
		$pattern_name = pattern_name($file); ?>

		<section class="pattern" id="<?php echo $pattern_name; ?>">
			<h1 class="pattern-title"><?php echo ucfirst($pattern_name); ?></h1>
			<?php include_once($patterns_dir.'/'.$file); ?>
			<details class="pattern-implementation">
				<summary>Show pattern code</summary>
					<fieldset>
						<legend>HTML</legend>
						<pre><code id="<?php echo $pattern_name; ?>-1"><?php echo htmlspecialchars(file_get_contents($patterns_dir.'/'.$file)); ?></code></pre>
					</fieldset>
					<fieldset>
						<legend>Sass</legend>
						<pre><code id="<?php echo $pattern_name; ?>-2"><?php echo htmlspecialchars(file_get_contents('assets/styles/patterns/_'.$pattern_name.'.scss')); ?></code></pre>
					</fieldset>
					<fieldset>
						<legend>JS</legend>
						<pre><code id="<?php echo $pattern_name; ?>-3"><?php echo htmlspecialchars(file_get_contents('assets/scripts/patterns/'.$pattern_name.'.js')); ?></code></pre>
					</fieldset>
			</details>
		</section>

	<?php endforeach; ?>
</main>

<?php
include_once 'includes/footer.php';
